package com.cristinaffmart.ementas.storage.entities

import androidx.room.*
import androidx.room.ForeignKey.CASCADE

@Entity(foreignKeys = [
    ForeignKey(
        onDelete = CASCADE, entity = StoredList::class,
        parentColumns = ["listId"], childColumns = ["listId"]
    )
])
data class StoredDish(
    @PrimaryKey(autoGenerate = true) val dishId: Long = 0,
    val name: String,
    val listId: Long
)