package com.cristinaffmart.ementas.storage

import android.content.Context
import androidx.hilt.work.HiltWorker
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import com.cristinaffmart.ementas.storage.entities.StoredList
import dagger.assisted.Assisted
import dagger.assisted.AssistedInject
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

@HiltWorker
class DatabaseWorker @AssistedInject constructor(private val ementasDatabase: EmentasDatabase,
                                                 @Assisted context: Context,
                                                 @Assisted workerParameters: WorkerParameters)
    : CoroutineWorker(context, workerParameters) {

    override suspend fun doWork(): Result = withContext(Dispatchers.IO) {
        try {
            val lists = listOf(
                StoredList(name = "List 1"),
                StoredList(name = "List 2"),
                StoredList(name = "List 3"),
                StoredList(name = "List 4"),
                StoredList(name = "List 5")
            )
            ementasDatabase.dishListDao().addLists(lists)
            Result.success()
        } catch (error: Exception) {
            Result.failure()
        }
    }
}