package com.cristinaffmart.ementas.storage.dao

import androidx.room.*
import com.cristinaffmart.ementas.storage.entities.StoredDish
import com.cristinaffmart.ementas.storage.entities.StoredList
import com.cristinaffmart.ementas.storage.entities.StoredListWithDishes
import kotlinx.coroutines.flow.Flow

@Dao
interface DishListDao {
    companion object {
        const val QUERY_GET_LIST_WITH_DISHES = "SELECT * FROM StoredList WHERE listId = :listId"
    }
    @Insert
    suspend fun addLists(lists: List<StoredList>)

    @Insert
    suspend fun addList(list: StoredList): Long

    @Update
    suspend fun updateList(list: StoredList)

    @Insert
    suspend fun addDishes(dishes: List<StoredDish>)

    @Delete
    suspend fun deleteDishes(dishes: List<StoredDish>)

    @Query("DELETE FROM StoredList WHERE listId = :listId")
    suspend fun deleteListById(listId: Long)

    @Query("SELECT * FROM StoredList")
    fun getLists(): Flow<List<StoredList>>

    @Transaction
    @Query(QUERY_GET_LIST_WITH_DISHES)
    fun getListWithDishesFlow(listId: Long): Flow<StoredListWithDishes>

    @Transaction
    @Query(QUERY_GET_LIST_WITH_DISHES)
    suspend fun getListWithDishes(listId: Long): StoredListWithDishes
}