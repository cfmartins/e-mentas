package com.cristinaffmart.ementas.storage

import com.cristinaffmart.ementas.storage.entities.StoredDish
import com.cristinaffmart.ementas.storage.entities.StoredList
import com.cristinaffmart.ementas.storage.entities.StoredListWithDishes
import kotlinx.coroutines.flow.Flow

interface StorageHelper {
    fun getLists(): Flow<List<StoredList>>
    suspend fun getListDishes(listId: Long): List<StoredDish>
    fun getListFlow(listId: Long): Flow<StoredListWithDishes>
    suspend fun addList(list: StoredList): Long
    suspend fun updateList(list: StoredList)
    suspend fun addDishes(dishes: List<StoredDish>)
    suspend fun deleteDishes(dishes: List<StoredDish>)
    suspend fun deleteList(listId: Long)
}