package com.cristinaffmart.ementas.storage

import androidx.room.Database
import androidx.room.RoomDatabase
import com.cristinaffmart.ementas.storage.dao.DishListDao
import com.cristinaffmart.ementas.storage.entities.StoredDish
import com.cristinaffmart.ementas.storage.entities.StoredList

@Database(
    entities = [
        StoredList::class,
        StoredDish::class],
    version = 1,
    exportSchema = false
)
abstract class EmentasDatabase : RoomDatabase() {
    abstract fun dishListDao(): DishListDao
}