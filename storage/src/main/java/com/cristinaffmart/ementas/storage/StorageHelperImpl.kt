package com.cristinaffmart.ementas.storage

import com.cristinaffmart.ementas.storage.dao.DishListDao
import com.cristinaffmart.ementas.storage.entities.StoredDish
import com.cristinaffmart.ementas.storage.entities.StoredList
import com.cristinaffmart.ementas.storage.entities.StoredListWithDishes
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class StorageHelperImpl @Inject constructor(private val dishListDao: DishListDao): StorageHelper {

    override fun getLists(): Flow<List<StoredList>> =
        dishListDao.getLists()

    override suspend fun getListDishes(listId: Long): List<StoredDish> =
        dishListDao.getListWithDishes(listId).dishes

    override fun getListFlow(listId: Long): Flow<StoredListWithDishes> =
        dishListDao.getListWithDishesFlow(listId)

    override suspend fun addList(list: StoredList): Long =
        dishListDao.addList(list)

    override suspend fun updateList(list: StoredList) {
        dishListDao.updateList(list)
    }

    override suspend fun addDishes(dishes: List<StoredDish>) {
        dishListDao.addDishes(dishes)
    }

    override suspend fun deleteDishes(dishes: List<StoredDish>) {
        dishListDao.deleteDishes(dishes)
    }

    override suspend fun deleteList(listId: Long) {
        dishListDao.deleteListById(listId)
    }
}
