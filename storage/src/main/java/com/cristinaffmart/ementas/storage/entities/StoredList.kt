package com.cristinaffmart.ementas.storage.entities

import androidx.room.*

@Entity
data class StoredList(
    @PrimaryKey(autoGenerate = true) val listId: Long = 0,
    val name: String,
    val onMonday: Boolean = false,
    val onTuesday: Boolean = false,
    val onWednesday: Boolean = false,
    val onThursday: Boolean = false,
    val onFriday: Boolean = false,
    val onSaturday: Boolean = false,
    val onSunday: Boolean = false
)

data class StoredListWithDishes(
    @Embedded
    val list: StoredList,
    @Relation(
        parentColumn = "listId",
        entityColumn = "listId",
    )
    val dishes: List<StoredDish>
)