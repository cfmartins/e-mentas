package com.cristinaffmart.ementas.storage

import android.content.Context
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import androidx.work.OneTimeWorkRequest
import androidx.work.WorkManager
import com.cristinaffmart.ementas.storage.dao.DishListDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object DatabaseModule {

    @Provides
    @Singleton
    fun provideDishListDao(ementasDatabase: EmentasDatabase): DishListDao =
        ementasDatabase.dishListDao()

    @Provides
    @Singleton
    fun provideEmentasDatabase(@ApplicationContext appContext: Context): EmentasDatabase {
        return Room.databaseBuilder(
            appContext,
            EmentasDatabase::class.java,
            "EmentasDatabase"
        ).addCallback(object: RoomDatabase.Callback() {
            override fun onCreate(db: SupportSQLiteDatabase) {
                super.onCreate(db)
                val request = OneTimeWorkRequest.from(DatabaseWorker::class.java)
                WorkManager.getInstance(appContext).enqueue(request)
            }
        }).build()
    }
}