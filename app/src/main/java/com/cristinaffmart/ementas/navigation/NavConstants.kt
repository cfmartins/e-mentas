package com.cristinaffmart.ementas.navigation

object NavConstants {
    const val MENU_SCREEN_ROUTE = "menu"
    const val DISHES_SCREEN_ROUTE = "dishes"
    const val CREATE_LIST_SCREEN_ROUTE = "createList"
    const val EDIT_LIST_SCREEN_ROUTE = "editList"

    const val LIST_ID_ARG = "listId"
}