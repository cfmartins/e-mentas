package com.cristinaffmart.ementas.navigation

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import com.cristinaffmart.ementas.R
import com.cristinaffmart.ementas.navigation.NavConstants.CREATE_LIST_SCREEN_ROUTE
import com.cristinaffmart.ementas.navigation.NavConstants.DISHES_SCREEN_ROUTE
import com.cristinaffmart.ementas.navigation.NavConstants.EDIT_LIST_SCREEN_ROUTE
import com.cristinaffmart.ementas.navigation.NavConstants.MENU_SCREEN_ROUTE

sealed class Screen(
    val route: String,
    @StringRes val screenName: Int,
    @DrawableRes val bottomNavIcon: Int? = null
) {
    object Menu : Screen(MENU_SCREEN_ROUTE, R.string.menu_screen_name, R.drawable.ic_calendar)
    object Dishes : Screen(DISHES_SCREEN_ROUTE, R.string.dishes_screen_name, R.drawable.ic_dish)
    object CreateList : Screen(CREATE_LIST_SCREEN_ROUTE, R.string.create_list_screen_name)
    object EditList : Screen(EDIT_LIST_SCREEN_ROUTE, R.string.edit_list_screen_name)
}
