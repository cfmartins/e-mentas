package com.cristinaffmart.ementas.view

import androidx.annotation.StringRes
import androidx.compose.material.AlertDialog
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import com.cristinaffmart.ementas.R

@Composable
fun EmentasAlertDialog(
    @StringRes title: Int,
    @StringRes message: Int,
    openDialog: MutableState<Boolean>,
    onPositiveAction: () -> Unit
) {
    AlertDialog(
        title = { Text(fontWeight = FontWeight.Bold,text = stringResource(id = title)) },
        text = { Text(text = stringResource(id = message)) },
        onDismissRequest = {},
        confirmButton = {
            TextButton(
                onClick = {
                    onPositiveAction.invoke()
                    openDialog.value = false
                },
                content = {
                    Text(
                        text = stringResource(id = R.string.button_yes),
                        color = MaterialTheme.colors.primary
                    )
                })
        },
        dismissButton = {
            TextButton(
                onClick = { openDialog.value = false },
                content = {
                    Text(
                        text = stringResource(id = R.string.button_no),
                        color = Color.Red
                    )
                })
        }
    )
}