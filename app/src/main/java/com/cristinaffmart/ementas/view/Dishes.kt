package com.cristinaffmart.ementas.view

import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons.Default
import androidx.compose.material.icons.filled.Delete
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.scale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import com.cristinaffmart.ementas.R
import com.cristinaffmart.ementas.navigation.NavConstants.CREATE_LIST_SCREEN_ROUTE
import com.cristinaffmart.ementas.navigation.NavConstants.EDIT_LIST_SCREEN_ROUTE
import com.cristinaffmart.ementas.storage.entities.StoredList
import com.cristinaffmart.ementas.ui.theme.Grey1
import com.cristinaffmart.ementas.viewmodel.DishListViewModel

@ExperimentalMaterialApi
@Composable
fun Dishes(navigationController: NavController, viewModel: DishListViewModel) {
    val lists = viewModel.getLists().collectAsState(initial = emptyList())
    Scaffold(
        modifier = Modifier.padding(bottom = 48.dp),
        content = {
            ListsList(
                lists = lists.value,
                onListClick = { listId ->
                    navigationController.navigate(route = "$EDIT_LIST_SCREEN_ROUTE/$listId")
                },
                onListSwipe = { listId ->
                    viewModel.deleteList(listId)
                })
        },
        floatingActionButton = {
            FloatingActionButton(content = {
                Icon(painter = painterResource(id = R.drawable.ic_add), contentDescription = "Add")
            }, onClick = {
                navigationController.navigate(CREATE_LIST_SCREEN_ROUTE)
            })
        }, floatingActionButtonPosition = FabPosition.End
    )
}

@ExperimentalMaterialApi
@Composable
fun ListsList(lists: List<StoredList>, onListClick: (Long) -> Unit, onListSwipe: (Long) -> Unit) {
    val openDialog = remember { mutableStateOf(false) }
    val listIdToDelete = remember { mutableStateOf(0L)}
    if (openDialog.value) {
        EmentasAlertDialog(title = R.string.delete_list_dialog_title,
            message = R.string.delete_list_dialog_message,
            openDialog = openDialog,
            onPositiveAction = {
                onListSwipe.invoke(listIdToDelete.value)
            })
    }
    LazyColumn(modifier = Modifier.padding(8.dp)) {
        items(lists) { list ->
            val dismissState = rememberDismissState(
                confirmStateChange = {
                    if (it == DismissValue.DismissedToStart) {
                        openDialog.value = true
                        listIdToDelete.value = list.listId
                    }
                    it != DismissValue.DismissedToStart
                }
            )
            SwipeToDismiss(
                state = dismissState,
                directions = setOf(DismissDirection.EndToStart),
                dismissThresholds = {
                    FractionalThreshold(0.25f)
                },
                background = {
                    val scale by animateFloatAsState(
                        if (dismissState.targetValue == DismissValue.Default) 0.75f else 1f
                    )
                    Box(
                        modifier = Modifier.fillMaxSize().padding(horizontal = 20.dp),
                        contentAlignment = Alignment.CenterEnd
                    ) {
                        Icon(
                            imageVector = Default.Delete,
                            contentDescription = "Delete",
                            modifier = Modifier.scale(scale)
                        )
                    }
                },
                dismissContent = {
                    ListItem(list, onListClick)
                }
            )
        }
    }
}

@ExperimentalMaterialApi
@Composable
fun ListItem(list: StoredList, onListClick: (Long) -> Unit) {
    Card(
        shape = RoundedCornerShape(4.dp),
        backgroundColor = Grey1,
        modifier = Modifier
            .padding(8.dp)
            .fillMaxWidth(),
        elevation = 4.dp,
        onClick = { onListClick.invoke(list.listId) }
    ) {
        Text(modifier = Modifier.padding(8.dp), text = list.name)
    }
}