package com.cristinaffmart.ementas.view

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Done
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight.Companion.Bold
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import com.cristinaffmart.ementas.R
import com.cristinaffmart.ementas.ui.theme.Grey2
import com.cristinaffmart.ementas.utils.WeekDay
import com.cristinaffmart.ementas.utils.Dish
import com.cristinaffmart.ementas.viewmodel.DishListViewModel

@Composable
fun CreateList(navigationController: NavController, viewModel: DishListViewModel, listId: Long = -1) {
    viewModel.fetchList(listId)
    val list by remember { viewModel.getListObservable() }.collectAsState(null)
    val shouldGoBack = viewModel.getGoBackToListsObservable().collectAsState()
    if (shouldGoBack.value) {
        navigationController.navigateUp()
    }
    Scaffold(
        topBar = {
            TopAppBar(
                title = { Text(stringResource(id = getScreenTitle(listId))) },
                actions = {
                    IconButton(
                        onClick = {
                            if (isEditingList(listId)) {
                                viewModel.editList(listId)
                            } else {
                                viewModel.saveList()
                            } },
                        content = {
                            Icon(imageVector = Icons.Default.Done, contentDescription = "Done")
                        })
                })
        }) {
        Column(modifier = Modifier.background(color = Grey2)) {
            ListName(listName = list?.name ?: "") {
                viewModel.setListName(it)
            }
            Title(title = stringResource(id = R.string.create_list_days_of_week_title))
            WeekDaysList(
                listWeekDays = list?.weekDays ?: emptyList(),
                onAddWeekDay = { viewModel.addWeekDay(it) },
                onRemoveWeekDay = { viewModel.removeWeekDay(it) }
            )
            Title(title = stringResource(id = R.string.create_list_dishes_title))
            ListDishes(
                dishes = list?.dishes ?: emptyList(),
                onAddNewDish = { viewModel.addNewDish(it) },
                onRemoveDish = { viewModel.removeDish(it) }
            )
        }
    }
}

private fun getScreenTitle(listId: Long): Int = if (isEditingList(listId)) {
    R.string.edit_list_screen_name
} else {
    R.string.create_list_screen_name
}

private fun isEditingList(listId: Long): Boolean = listId != -1L

@Composable
private fun ListName(listName: String, onListNameChange: (String) -> Unit) {
    Column(modifier = Modifier.padding(16.dp)) {
        OutlinedTextField(
            modifier = Modifier
                .fillMaxWidth()
                .padding(bottom = 16.dp),
            value = listName,
            onValueChange = onListNameChange,
            label = { Text(text = stringResource(R.string.create_list_name_hint)) }
        )
    }
}

@Composable
fun Title(title: String) {
    Text(
        modifier = Modifier.padding(start = 16.dp, bottom = 8.dp),
        fontSize = 16.sp,
        fontWeight = Bold,
        text = title
    )
}

@Composable
fun WeekDaysList(listWeekDays: List<WeekDay>,
                 onAddWeekDay: (WeekDay) -> Unit,
                 onRemoveWeekDay: (WeekDay) -> Unit) {
    Column(modifier = Modifier.padding(16.dp)) {
        Row {
            val daysOfWeek = WeekDay.values()
            Column(modifier = Modifier.padding(end = 16.dp)) {
                daysOfWeek.slice(0 until 4).forEach {
                    WeekDaysWithCheckBox(
                        weekDay = it,
                        listWeekDays = listWeekDays,
                        onAddWeekDay = onAddWeekDay,
                        onRemoveWeekDay = onRemoveWeekDay)
                }
            }
            Column {
                daysOfWeek.slice(4 until daysOfWeek.size).forEach {
                    WeekDaysWithCheckBox(
                        weekDay = it,
                        listWeekDays = listWeekDays,
                        onAddWeekDay = onAddWeekDay,
                        onRemoveWeekDay = onRemoveWeekDay)
                }
            }
        }
    }
}

@Composable
fun WeekDaysWithCheckBox(weekDay: WeekDay,
                         listWeekDays: List<WeekDay>,
                         onAddWeekDay: (WeekDay) -> Unit,
                         onRemoveWeekDay: (WeekDay) -> Unit) {
    Row(modifier = Modifier.padding(top = 8.dp, bottom = 8.dp)) {
        Checkbox(
            modifier = Modifier.padding(end = 4.dp),
            checked = listWeekDays.contains(weekDay),
            onCheckedChange = { isChecked ->
                if (isChecked) {
                    if (listWeekDays.contains(weekDay).not()) {
                        onAddWeekDay(weekDay)
                    }
                } else {
                    onRemoveWeekDay(weekDay)
                }
            }
        )
        WeekDayText(weekDay = weekDay)
    }
}

@Composable
fun WeekDayText(weekDay: WeekDay) {
    Text(text = stringResource(id = weekDay.textId))
}

@Composable
fun ListDishes(dishes: List<Dish>, onAddNewDish: (String) -> Unit, onRemoveDish: (Dish) -> Unit) {
    Column(modifier = Modifier
        .background(Color.White)
        .padding(16.dp)) {
        LazyColumn {
            items(dishes) { dish ->
                DishItem(dish = dish, onRemoveDish)
            }
        }
        var dishName by remember { mutableStateOf(TextFieldValue("")) }
        val isNameValid = dishName.text.isNotEmpty()
        Row(modifier = Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.SpaceBetween,
            verticalAlignment = Alignment.CenterVertically) {
            TextField(
                colors = TextFieldDefaults.textFieldColors(backgroundColor = Color.White),
                value = dishName,
                onValueChange = { dishName = it })
            IconButton(
                enabled = isNameValid,
                onClick = {
                    onAddNewDish.invoke(dishName.text)
                    dishName = TextFieldValue("")
                }) {
                Icon(painter = painterResource(R.drawable.ic_add_dish), contentDescription = "Add")
            }
        }
    }
}

@Composable
fun DishItem(dish: Dish, onRemoveDish: (Dish) -> Unit) {
    Row(modifier = Modifier.fillMaxWidth(),
        horizontalArrangement = Arrangement.SpaceBetween,
        verticalAlignment = Alignment.CenterVertically) {
        Text(modifier = Modifier.padding(8.dp), text = dish.name)
        IconButton(onClick = { onRemoveDish.invoke(dish) }) {
            Icon(painter = painterResource(R.drawable.ic_remove_dish), contentDescription = "Remove")
        }
    }
    Divider(color = Grey2, thickness = 1.dp)
}