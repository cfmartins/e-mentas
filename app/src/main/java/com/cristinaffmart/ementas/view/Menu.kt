package com.cristinaffmart.ementas.view

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight.Companion.Bold
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import com.cristinaffmart.ementas.R
import com.cristinaffmart.ementas.ui.theme.Grey1
import com.cristinaffmart.ementas.utils.MenuDay
import com.cristinaffmart.ementas.utils.toShortDate
import com.cristinaffmart.ementas.utils.toWeekDayDate
import com.cristinaffmart.ementas.viewmodel.MenuViewModel
import com.google.accompanist.pager.ExperimentalPagerApi
import com.google.accompanist.pager.HorizontalPager
import com.google.accompanist.pager.rememberPagerState

@ExperimentalMaterialApi
@ExperimentalPagerApi
@Composable
fun Menu(navigationController: NavController, viewModel: MenuViewModel) {

    val menuWeeks = viewModel.getMenuWeeks()
    val pagerState = rememberPagerState(pageCount = menuWeeks.size)

    HorizontalPager(
        state = pagerState,
        modifier = Modifier.padding(16.dp)) { page ->
        val menuWeek = menuWeeks[page]
        Column(verticalArrangement = Arrangement.SpaceBetween) {
            Row(
                modifier = Modifier.fillMaxWidth(),
                horizontalArrangement = Arrangement.SpaceBetween
            ) {
                Icon(painter = painterResource(R.drawable.ic_arrow_left), contentDescription = "Left")
                Text(
                    text = "${menuWeek.week.startDate.toShortDate()} - ${menuWeek.week.endDate.toShortDate()}",
                    fontWeight = Bold,
                )
                Icon(painter = painterResource(R.drawable.ic_arrow_right), contentDescription = "Right")
            }
            LazyColumn {
                items(menuWeek.menuDays) {
                    Day(menuDay = it)
                }
            }
        }
    }
}

@ExperimentalMaterialApi
@Composable
fun Day(menuDay: MenuDay) {
    Text(modifier = Modifier.padding(8.dp), text = menuDay.date.toWeekDayDate(), fontWeight = Bold)
    MealCard(
        mealIcon = painterResource(id = R.drawable.ic_sun),
        mealName = stringResource(id = R.string.meal_lunch),
        dishName = menuDay.lunchDish.name)
    MealCard(
        mealIcon = painterResource(id = R.drawable.ic_moon),
        mealName = stringResource(id = R.string.meal_dinner),
        dishName = menuDay.dinnerDish.name)
}

@ExperimentalMaterialApi
@Composable
fun MealCard(mealIcon: Painter, mealName: String, dishName: String) {
    Card(
        shape = RoundedCornerShape(4.dp),
        backgroundColor = Grey1,
        modifier = Modifier
            .padding(8.dp)
            .fillMaxWidth(),
        elevation = 4.dp,
        onClick = { }
    ) {
        Column {
            Row(modifier = Modifier.padding(8.dp)) {
                Text(text = mealName, modifier = Modifier.padding(end = 16.dp))
                Icon(painter = mealIcon, contentDescription = mealName)
            }
            Text(modifier = Modifier.padding(8.dp), text = dishName)
        }
    }
}