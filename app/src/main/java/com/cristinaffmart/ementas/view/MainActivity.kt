package com.cristinaffmart.ementas.view

import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavDestination.Companion.hierarchy
import androidx.navigation.NavGraph.Companion.findStartDestination
import androidx.navigation.NavType
import androidx.navigation.compose.*
import com.cristinaffmart.ementas.R
import com.cristinaffmart.ementas.navigation.NavConstants.LIST_ID_ARG
import com.cristinaffmart.ementas.navigation.Screen
import com.cristinaffmart.ementas.ui.theme.EMentasTheme
import com.cristinaffmart.ementas.viewmodel.DishListViewModel
import com.cristinaffmart.ementas.viewmodel.MenuViewModel
import com.google.accompanist.pager.ExperimentalPagerApi
import dagger.hilt.android.AndroidEntryPoint

@ExperimentalPagerApi
@ExperimentalMaterialApi
@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            EMentasTheme {
                val navController = rememberNavController()
                val items = listOf(Screen.Menu, Screen.Dishes)
                Scaffold(
                    bottomBar = {
                        BottomNavigation {
                            val navBackStackEntry by navController.currentBackStackEntryAsState()
                            val currentRoute = navBackStackEntry?.destination
                            items.forEach { screen ->
                                BottomNavigationItem(
                                    icon = {
                                        Icon(painterResource(id = screen.bottomNavIcon ?: R.drawable.ic_dish),
                                            screen.route)
                                    },
                                    label = { Text(stringResource(screen.screenName)) },
                                    selected = currentRoute?.hierarchy?.any { it.route == screen.route } == true,
                                    onClick = {
                                        navController.navigate(screen.route) {
                                            popUpTo(navController.graph.findStartDestination().id) {
                                                saveState = true
                                            }
                                            launchSingleTop = true
                                            restoreState = true
                                        }
                                    }
                                )
                            }
                        }
                    }
                ) {
                    NavHost(navController, startDestination = Screen.Menu.route) {
                        composable(Screen.Menu.route) {
                            val menuViewModel = hiltViewModel<MenuViewModel>()
                            Menu(navController, menuViewModel)
                        }
                        composable(Screen.Dishes.route) {
                            val dishListViewModel = hiltViewModel<DishListViewModel>()
                            Dishes(navController, dishListViewModel) }
                        composable(Screen.CreateList.route) {
                            val dishListViewModel = hiltViewModel<DishListViewModel>()
                            CreateList(navController, dishListViewModel)
                        }
                        composable("${Screen.EditList.route}/{$LIST_ID_ARG}",
                            arguments = listOf(navArgument(LIST_ID_ARG) {
                                type = NavType.LongType
                            })) {
                            val dishListViewModel = hiltViewModel<DishListViewModel>()
                            CreateList(
                                navigationController = navController,
                                viewModel = dishListViewModel,
                                listId = it.arguments?.getLong(LIST_ID_ARG) ?: 0
                            )
                        }
                    }
                }
            }
        }
    }
}

@Composable
fun Greeting(name: String) {
    Text(text = "Hello $name!")
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    EMentasTheme {
        Greeting("Android")
    }
}