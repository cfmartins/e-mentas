package com.cristinaffmart.ementas.utils

import org.threeten.bp.LocalDate

data class MenuDay(val date: LocalDate,
                   val lunchDish: Dish,
                   val dinnerDish: Dish)
