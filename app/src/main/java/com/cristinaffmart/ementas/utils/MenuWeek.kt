package com.cristinaffmart.ementas.utils

import kotlin.collections.List

// TODO Replace with real representation of a week
data class MenuWeek(val week: Week,
                    val menuDays: List<MenuDay>)
