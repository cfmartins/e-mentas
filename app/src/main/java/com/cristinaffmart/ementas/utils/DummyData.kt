package com.cristinaffmart.ementas.utils

import org.threeten.bp.LocalDate

object DummyData {
    val menuDays = listOf(
        MenuDay(
            date = LocalDate.now(),
            lunchDish = Dish(name = "Bacalhau com natas"),
            dinnerDish = Dish(name = "Pizza")
        ),
        MenuDay(
            date = LocalDate.now().plusDays(1),
            lunchDish = Dish(name = "Massa com tomate"),
            dinnerDish = Dish(name = "Arroz de carne")
        ),
        MenuDay(
            date = LocalDate.now().plusDays(2),
            lunchDish = Dish(name = "Arroz de carne"),
            dinnerDish = Dish(name = "Picanha")
        ),
        MenuDay(
            date = LocalDate.now().plusDays(3),
            lunchDish = Dish(name = "Massa carbonara"),
            dinnerDish = Dish(name = "Peixe assado")
        ),
        MenuDay(
            date = LocalDate.now().plusDays(4),
            lunchDish = Dish(name = "Peixe assado"),
            dinnerDish = Dish(name = "Arroz de frango")
        ),
        MenuDay(
            date = LocalDate.now().plusDays(5),
            lunchDish = Dish(name = "Arroz de frango"),
            dinnerDish = Dish(name = "Pescada cozida")
        ),
        MenuDay(
            date = LocalDate.now().plusDays(6),
            lunchDish = Dish(name = "Pescada cozida"),
            dinnerDish = Dish(name = "Rancho")
        )
    )
}