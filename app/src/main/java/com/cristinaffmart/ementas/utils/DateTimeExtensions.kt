package com.cristinaffmart.ementas.utils

import org.threeten.bp.LocalDate
import org.threeten.bp.format.DateTimeFormatter
import org.threeten.bp.format.FormatStyle

fun LocalDate.toWeekDayDate(): String =
    format(DateTimeFormatter.ofLocalizedDate(FormatStyle.FULL))

fun LocalDate.toShortDate(): String =
    format(DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM))