package com.cristinaffmart.ementas.utils

import kotlin.collections.List

data class List(val name: String = "",
                val weekDays: List<WeekDay> = emptyList(),
                val dishes: List<Dish> = emptyList())
