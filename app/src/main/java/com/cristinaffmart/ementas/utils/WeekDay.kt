package com.cristinaffmart.ementas.utils

import androidx.annotation.StringRes
import com.cristinaffmart.ementas.R

enum class WeekDay(@StringRes val textId: Int) {
    Monday(R.string.week_day_monday),
    Tuesday(R.string.week_day_tuesday),
    Wednesday(R.string.week_day_wednesday),
    Thursday(R.string.week_day_thursday),
    Friday(R.string.week_day_friday),
    Saturday(R.string.week_day_saturday),
    Sunday(R.string.week_day_sunday)
}