package com.cristinaffmart.ementas.utils

import org.threeten.bp.LocalDate

data class Week(val startDate: LocalDate,
                val endDate: LocalDate)
