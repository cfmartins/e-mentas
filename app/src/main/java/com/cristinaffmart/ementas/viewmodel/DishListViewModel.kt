package com.cristinaffmart.ementas.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.cristinaffmart.ementas.storage.StorageHelper
import com.cristinaffmart.ementas.storage.entities.StoredDish
import com.cristinaffmart.ementas.storage.entities.StoredList
import com.cristinaffmart.ementas.storage.entities.StoredListWithDishes
import com.cristinaffmart.ementas.utils.Dish
import com.cristinaffmart.ementas.utils.WeekDay
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class DishListViewModel @Inject constructor(private val storageHelper: StorageHelper): ViewModel() {
    private val listStateFlow = MutableStateFlow(value = com.cristinaffmart.ementas.utils.List())
    private val goBackToListsStateFlow = MutableStateFlow(value = false)

    fun getLists(): Flow<List<StoredList>> = storageHelper.getLists()

    fun getListObservable(): Flow<com.cristinaffmart.ementas.utils.List> = listStateFlow

    fun fetchList(listId: Long) {
        if (listId != -1L) {
            viewModelScope.launch {
                storageHelper.getListFlow(listId).collect { storedListWithDishes ->
                    listStateFlow.value = com.cristinaffmart.ementas.utils.List(
                        name = storedListWithDishes.list.name,
                        weekDays = getWeekDaysFromList(storedListWithDishes),
                        dishes = storedListWithDishes.dishes.map { Dish(name = it.name) }
                    )
                }
            }
        }
    }

    private fun getWeekDaysFromList(list: StoredListWithDishes): List<WeekDay> {
        val weekDays = mutableListOf<WeekDay>()
        list.list.let {
            if (it.onMonday) {
                weekDays.add(WeekDay.Monday)
            }
            if (it.onTuesday) {
                weekDays.add(WeekDay.Tuesday)
            }
            if (it.onWednesday) {
                weekDays.add(WeekDay.Wednesday)
            }
            if (it.onThursday) {
                weekDays.add(WeekDay.Thursday)
            }
            if (it.onFriday) {
                weekDays.add(WeekDay.Friday)
            }
            if (it.onSaturday) {
                weekDays.add(WeekDay.Saturday)
            }
            if (it.onSunday) {
                weekDays.add(WeekDay.Sunday)
            }
        }
        return weekDays
    }

    fun setListName(name: String) {
        listStateFlow.value = listStateFlow.value.copy(name = name)
    }

    fun addWeekDay(weekDay: WeekDay) {
        val weekDays = listStateFlow.value.weekDays.toMutableList()
        weekDays.add(weekDay)
        listStateFlow.value = listStateFlow.value.copy(weekDays = weekDays)
    }

    fun removeWeekDay(weekDay: WeekDay) {
        val weekDays = listStateFlow.value.weekDays.toMutableList()
        weekDays.remove(weekDay)
        listStateFlow.value = listStateFlow.value.copy(weekDays = weekDays)
    }

    fun addNewDish(dishName: String) {
        val dishes = listStateFlow.value.dishes.toMutableList()
        dishes.add(Dish(name = dishName))
        listStateFlow.value = listStateFlow.value.copy(dishes = dishes)
    }

    fun removeDish(dish: Dish) {
        val dishes = listStateFlow.value.dishes.toMutableList()
        dishes.remove(dish)
        listStateFlow.value = listStateFlow.value.copy(dishes = dishes)
    }

    fun saveList() {
        val list = StoredList(
            name = listStateFlow.value.name,
            onMonday = listStateFlow.value.weekDays.contains(WeekDay.Monday),
            onTuesday = listStateFlow.value.weekDays.contains(WeekDay.Tuesday),
            onWednesday = listStateFlow.value.weekDays.contains(WeekDay.Wednesday),
            onThursday = listStateFlow.value.weekDays.contains(WeekDay.Thursday),
            onFriday = listStateFlow.value.weekDays.contains(WeekDay.Friday),
            onSaturday = listStateFlow.value.weekDays.contains(WeekDay.Saturday),
            onSunday = listStateFlow.value.weekDays.contains(WeekDay.Sunday))
        viewModelScope.launch {
            try {
                val listId = storageHelper.addList(list)
                val storedDishes = listStateFlow.value.dishes.map {
                    StoredDish(name = it.name, listId = listId)
                }
                storageHelper.addDishes(storedDishes)
                goBackToListsStateFlow.value = true
            } catch (error: Exception) {
                goBackToListsStateFlow.value = false
            }
        }
    }

    fun editList(listId: Long) {
        val list = StoredList(
            listId = listId,
            name = listStateFlow.value.name,
            onMonday = listStateFlow.value.weekDays.contains(WeekDay.Monday),
            onTuesday = listStateFlow.value.weekDays.contains(WeekDay.Tuesday),
            onWednesday = listStateFlow.value.weekDays.contains(WeekDay.Wednesday),
            onThursday = listStateFlow.value.weekDays.contains(WeekDay.Thursday),
            onFriday = listStateFlow.value.weekDays.contains(WeekDay.Friday),
            onSaturday = listStateFlow.value.weekDays.contains(WeekDay.Saturday),
            onSunday = listStateFlow.value.weekDays.contains(WeekDay.Sunday))
        viewModelScope.launch {
            try {
                val oldListDishes = storageHelper.getListDishes(listId)
                val newListDishes = listStateFlow.value.dishes
                val dishesToAdd = mutableListOf<StoredDish>()
                newListDishes.forEach { dish ->
                    if (oldListDishes.find { it.name == dish.name } == null) {
                        dishesToAdd.add(StoredDish(name = dish.name, listId = listId))
                    }
                }
                storageHelper.addDishes(dishesToAdd)
                val dishesToDelete = mutableListOf<StoredDish>()
                oldListDishes.forEach { storedDish ->
                    if (newListDishes.find { it.name == storedDish.name } == null) {
                        dishesToDelete.add(storedDish)
                    }
                }
                storageHelper.deleteDishes(dishesToDelete)
                storageHelper.updateList(list)
                goBackToListsStateFlow.value = true
            } catch (error: Exception) {
                goBackToListsStateFlow.value = false
            }
        }
    }

    fun getGoBackToListsObservable(): StateFlow<Boolean> = goBackToListsStateFlow

    fun deleteList(listId: Long) {
        viewModelScope.launch {
            storageHelper.deleteList(listId)
        }
    }
}