package com.cristinaffmart.ementas.viewmodel

import androidx.lifecycle.ViewModel
import com.cristinaffmart.ementas.storage.StorageHelper
import com.cristinaffmart.ementas.utils.DummyData
import com.cristinaffmart.ementas.utils.MenuWeek
import com.cristinaffmart.ementas.utils.Week
import dagger.hilt.android.lifecycle.HiltViewModel
import org.threeten.bp.DayOfWeek
import org.threeten.bp.LocalDate
import javax.inject.Inject

@HiltViewModel
class MenuViewModel @Inject constructor(private val storageHelper: StorageHelper): ViewModel() {
    fun getMenuWeeks(): List<MenuWeek> =
        listOf(MenuWeek(week = getWeekOfDate(), menuDays = DummyData.menuDays))

    private fun getWeekOfDate(date: LocalDate = LocalDate.now()): Week {
        val currentDayOfWeekIndex = date.dayOfWeek.value.minus(1).toLong()
        val mondayDate = date.minusDays(currentDayOfWeekIndex)
        val sundayDayOfWeekIndex = DayOfWeek.SUNDAY.value.minus(1).toLong()
        val sundayDate = mondayDate.plusDays(sundayDayOfWeekIndex)
        return Week(startDate = mondayDate, endDate = sundayDate)
    }
}